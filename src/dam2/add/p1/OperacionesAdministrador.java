package dam2.add.p1;

import java.io.*;
import java.util.*;

import static dam2.add.p1.OperacionesUsuario.escribirLog;

public class OperacionesAdministrador {
    // muestra el menu que tiene solo el administrador
    public static void mostrarMenuAdmin() {

        Scanner key = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir) {
            System.out.println("1. Desbloquear Usuario");
            System.out.println("2. Mostrar Lista de usuarios bloqueados");
            System.out.println("3. Salir");

            try {
                System.out.println("Elija una de las opciones");
                opcion = key.nextInt();

                switch (opcion) {
                    case 1:
                        desbloquearUsuario();
                        break;
                    case 2:
                        mostrarBloqueos();
                        break;
                    case 3:
                        salir = true;
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Introduce un n�mero del 1 al 3");
                }
            } catch (InputMismatchException e) {
                System.out.println();
                key.next();
            }
        }

    }

    // Pide el nick del usuario a desbloquear y llama a otro m�todo para que lo
    // borre de la lista
    private static void desbloquearUsuario() {
        Scanner key = new Scanner(System.in);
        String nick;
        System.out.println("Introduzca el nick del usuario");
        nick = key.nextLine();
        ActualizarLista(nick);
    }

    // Borra al usuario y actualiza la lista
    private static void ActualizarLista(String nick) {

        ArrayList<String> bloqueados = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader("ficheros/bloqueado.txt"));
            String linea;
            while ((linea = br.readLine()) != null) {
                bloqueados.add(linea);
            }
            br.close();
        } catch (IOException e) {
            System.out.println("Error al actualizar lista");
            escribirLog("Error al actualizar la lista");
        }
        bloqueados.remove(nick);

        String[] nuevaLista = new String[bloqueados.size()];
        nuevaLista = bloqueados.toArray(nuevaLista);

        try {
            FileWriter bloqueoActualizado = new FileWriter("ficheros/bloqueado.txt");
            for (String s : nuevaLista) {
                bloqueoActualizado.write(s);
            }
            escribirLog("Lista actualizada");
            bloqueoActualizado.close();
        } catch (IOException e) {
            System.out.println("Error al actualizar la lista");
        }

    }

    // Muestra la lista de usuarios bloqueados
    private static void mostrarBloqueos() {
        String nickname;
        try {
            FileReader bloqueados = new FileReader("ficheros/bloqueado.txt");
            BufferedReader br = new BufferedReader(bloqueados);
            while ((nickname = br.readLine()) != null) {
                System.out.println(nickname);
                escribirLog("lista de usuarios bloqueados");
            }
            br.close();
        } catch (IOException ignored) {
            System.out.println("Error al mostrar la lista de bloqueados");
            escribirLog("Error al mostrar la lista de bloqueados");
        }
    }
}