package dam2.add.p1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        mostrarMenu();

    }
    // Muestra el menu al usuario desde el que llama a otros m�todos para hacerlo
    // que el usuario le pide
	private static void mostrarMenu() {

        Scanner key = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir) {
            System.out.println("1. Nuevo Usuario");
            System.out.println("2. Entrar");
            System.out.println("3. Salir");

            try {
                System.out.println("Elija una de las opciones");
                opcion = key.nextInt();

                switch (opcion) {
                    case 1:
                        OperacionesUsuario.registrarUsuario();
                        break;
                    case 2:
                        OperacionesUsuario.loguearse();
                        break;
                    case 3:
                        salir = true;
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Introduce un n�mero del 1 al 3");
                }
            } catch (InputMismatchException e) {
                System.out.println();
                key.next();
            }
        }
    }
}