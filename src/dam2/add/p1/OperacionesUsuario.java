package dam2.add.p1;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class OperacionesUsuario {

    //Me comprueba si el usuario existe
    private static boolean comprobarUsuario(String user, String pass) {
        Scanner entrada = null;
        String linea;
        boolean contiene = false;

        try {
            File acceso = new File("ficheros/acceso.txt");
            entrada = new Scanner(acceso);
            while (entrada.hasNext()) {
                linea = entrada.nextLine();

                if (linea.contains(user)) {
                    contiene = true;
                    break;
                }

            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            System.out.println("Error usuario no encontrado");
            escribirLog("Error al comprobar usuario");
        }
        return contiene;

    }

    //Me comprueba si la contraseņa es la correcta con respecto al usuario introducido
    private static boolean comprobarPassword(String pass, String user) {
        Scanner entrada = null;
        String linea;
        boolean contiene = false;
        try {
            File acceso = new File("ficheros/acceso.txt");
            entrada = new Scanner(acceso);
            while (entrada.hasNext()) {
                linea = entrada.nextLine();

                if (linea.contains(user + ":" + pass + ".")) {
                    contiene = true;
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            System.out.println("Error contraseņa no coincide");
            escribirLog("Error contraseņa no coincide");
        }
        return contiene;
    }

    //Me registra un nuevo usuario
    public static void registrarUsuario() {
        String user = "";
        String pass = "";
        String pass2 = "";
        Scanner key = new Scanner(System.in);
        boolean correcto = true;

        do {

            System.out.println("Introduzca un nombre de usuario");
            user = key.nextLine();

            System.out.println("Introduzca una contraseņa");
            pass = key.nextLine();

            System.out.println("Confirme la contraseņa");
            pass2 = key.nextLine();

            if (comprobarUsuario(user, pass)) {
                System.out.println("Ese nombre de usuario ya esta en uso por favor elija otro");
                correcto = false;
                escribirLog("Nick ya registrado");
            } else if (!pass.equals(pass2)) {
                System.out.println("Las contraseņas no coinciden");
                escribirLog("las contraseņas no coinciden");
                correcto = false;
            } else {

                try {
                    FileWriter fw = new FileWriter("ficheros/acceso.txt", true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(user + ":" + pass + ".");
                    bw.newLine();
                    bw.flush();
                    escribirLog(user + " registrado correctamente");

                } catch (IOException e) {
                    System.out.println("Error al registrar usuario");
                    escribirLog("Error al registrar al usuario");
                }
                correcto = true;
            }
        } while (!correcto);
    }

    //Loguea al usuario y comprueba si el usuario esta registrado, bloqueado,
    // o lo bloquea si pasa de intentos llamando a otros metodos
    public static void loguearse() {
        String user = "";
        String pass = "";


        Scanner key = new Scanner(System.in);

        System.out.println("Introduzca usuario");
        user = key.nextLine();

        boolean bloqueado = false;
        int intentos = 0;
        do {

            System.out.println("Introduzca la contraseņa");
            pass = key.nextLine();

            if (getBloqueado(user)) {
                System.out.println("Usuario bloqueado");
                escribirLog(user + " bloqueado");
                bloqueado = true;
            } else if ((comprobarUsuario(user, pass)) && (comprobarPassword(pass, user))) {
                System.out.println("Hola " + user);
                escribirLog(user + " Logueado correctamente");
                if (user.equals("admin")) {
                    OperacionesAdministrador.mostrarMenuAdmin();
                } else {
                    bloqueado = true;
                }
            } else if ((comprobarUsuario(user, pass)) && (!comprobarPassword(pass, user))) {
                System.out.println("Contraseņa incorrecta");
                intentos++;
                bloqueado = false;
                if ((intentos == 2) && (!user.equals("admin"))) {
                    bloquearUsuario(user);
                }
            }else if (!comprobarUsuario(user,pass)){
                System.out.println("El usuario no existe por favor cree un nuevo usuario");
                registrarUsuario();
            }
        } while (!bloqueado);

    }

    //Bloquea al usuario metiendolo en otro fichero//
    private static void bloquearUsuario(String user) {

        try {
            FileWriter fw = new FileWriter("ficheros/bloqueado.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(user);
            bw.newLine();
            bw.flush();
            escribirLog(user + " se ha bloqueado");

        } catch (IOException e) {
            System.out.println("Error al bloquear al usuario");
            escribirLog("Error al bloquear al usuario");
        }
    }

    //Me comprueba si el usuario esta en la lista de bloqueados
    private static boolean getBloqueado(String user) {
        Scanner entrada = null;
        String linea;
        boolean contiene = false;
        try {
            File bloqueo = new File("ficheros/bloqueado.txt");
            entrada = new Scanner(bloqueo);
            while (entrada.hasNext()) {
                linea = entrada.nextLine();

                if (linea.contains(user)) {
                    contiene = true;
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return contiene;
    }

    //Escribe el registro de lo que va sucediendo
    public static void escribirLog(String aviso) {

        File login = new File("ficheros/Login.txt");
        try {
            Date fecha = new Date();
            DateFormat formatoFechaHora = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
            FileWriter fw = new FileWriter(login, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(formatoFechaHora.format(fecha) + " " + aviso);
            bw.newLine();
            bw.flush();
        } catch (IOException e) {
            System.out.println("Fallo en el fichero login");
        }
    }
}


